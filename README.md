# user-regional-2023.io

Original repo for community events dashboard, superseded by https://gitlab.com/rconf/community-events-2023.

This remains just to redirect any old links to the new URL.